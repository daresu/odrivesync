#include "syncfiledialog.h"
#include "ui_syncfiledialog.h"
#include <QFileDialog>
#include <QDebug>

SyncFileDialog::SyncFileDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SyncFileDialog)
{
    ui->setupUi(this);
}

SyncFileDialog::~SyncFileDialog()
{
    delete ui;
}

QString SyncFileDialog::getFileName()
{
    return m_filename;
}

bool SyncFileDialog::getRecursiveFlg()
{
    return m_recursiveFlg;
}

void SyncFileDialog::setOdriveSyncPath(QString odrivesyncpath)
{
    m_odrivesyncpath = odrivesyncpath;
}

void SyncFileDialog::on_buttonBox_accepted()
{
    m_filename = ui->txtPathSync->text();
    if (ui->chkRecursive->checkState() == Qt::Checked)
    {
        m_recursiveFlg = true;
    } else {
        m_recursiveFlg = false;
    }

    this->close();
}

void SyncFileDialog::on_buttonBox_rejected()
{
    this->close();
}

void SyncFileDialog::on_btnSearch_clicked()
{
    QString fileName =
            QFileDialog::getOpenFileName(this,
                                         tr("同期したいフォルダまたはファイルを選択してください"),
                                         m_odrivesyncpath,
                                         tr("同期ファイル (*.cloudf *.cloud)"));
    qDebug() << fileName;

    if(!fileName.isEmpty()){
        ui->txtPathSync->setText(fileName);
    }
}
