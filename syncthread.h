#ifndef SYNCTHREAD_H
#define SYNCTHREAD_H

#include <QThread>
#include <QList>
#include <QMutex>
#include "fileinfo.h"

class SyncThread : public QThread
{
    Q_OBJECT
public:
    explicit SyncThread(QObject *parent = 0);
    void stop();// スレッドを止める
    QString messageStr;// スレッドが走っているかの状態メッセージ格納
    QString odrivePath;
    void list_push(QString fileName, bool recursiveFlg);
    FileInfo list_get();
    bool list_isEmpty();


protected:
    void run() override ;// スレッドを始める関数（再実装）

private:
    volatile bool stopped;// volatileは処理系の最適化の抑制の意味
    mutable QMutex mutex;
    mutable QMutex list_mutex;
    QList<FileInfo> syncFileList;   // 同期ファイルのリスト
    QString const logfile = "sync.log";
};

#endif // SYNCTHREAD_H
