#ifndef SYNCFILEDIALOG_H
#define SYNCFILEDIALOG_H

#include <QDialog>

namespace Ui {
class SyncFileDialog;
}

class SyncFileDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SyncFileDialog(QWidget *parent = 0);
    ~SyncFileDialog();

    QString getFileName();
    bool getRecursiveFlg();
    void setOdriveSyncPath(QString odrivesyncpath);

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_btnSearch_clicked();

private:
    Ui::SyncFileDialog *ui;
    QAction *showWidget;
    QString m_filename;
    bool m_recursiveFlg;
    QString m_odrivesyncpath;
};

#endif // SYNCFILEDIALOG_H
