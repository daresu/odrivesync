#include "widget.h"
#include <QApplication>
#include <QMessageBox>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // システムトレイが動作するか確認
    if(!QSystemTrayIcon::isSystemTrayAvailable())
    {
        qErrnoWarning(1, "このシステム上では動作できません。");
        return 1;
    }

    // ウィンドウが閉じられても、アプリは終了されない
    QApplication::setQuitOnLastWindowClosed(false);
    Widget w;
//    w.show();

    return a.exec();
}
