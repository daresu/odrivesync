#include "syncthread.h"
#include <QDebug>
#include <QProcess>
#include <QFile>
#include <QDirIterator>
#include <QStandardPaths>

/*!
 * \brief コンストラクタ
 * \param parent
 */
SyncThread::SyncThread(QObject *parent):
    QThread(parent)
{
    messageStr = "Thread Stop!";
    stopped = false;
}

/*!
 * \brief スレッドのメイン
 */
void SyncThread::run()//QThreadのrunの再実装
{
    bool first = true;

    // 同期したファイルを書き込むファイル
    QString logfilePath = QStandardPaths::writableLocation(QStandardPaths::DataLocation) + "/" + logfile;

    QFile appendFile(logfilePath);
    // 開くことができなければ警告を出す
    if (!appendFile.open(QIODevice::Append))
    {
        qWarning("ログファイルを開けませんでした。");
    }
    QTextStream out(&appendFile);

    while(!stopped)//スレッドがrun状態ならループという意味
    {
        {
            QMutexLocker locker(&mutex);

            if(first)//ループの最初の一回だけmessageStrを変更するという意味
            {
                messageStr = "Thread Running!";//run中なら
                first = false;
            }

            if(QFile::exists(odrivePath)){
                // リストに同期ファイルがあった場合に同期処理を行う
                if(!list_isEmpty()) {

                    // リストから同期ファイルを取り出す
                    FileInfo info = list_get();
                    messageStr = info.getFileName() + "を同期中"; // run中なら

                    // ファイルを同期する
                    QProcess *odriveProcess = new QProcess(this);
                    qDebug() << odrivePath + " sync " + info.getFileName();
                    QString syncFile = "\"" + info.getFileName() + "\"";
                    odriveProcess->start(odrivePath + " sync " + syncFile);
                    // 完了するまで待つ
                    if (!odriveProcess->waitForFinished(600000)) {
                        qWarning() << odriveProcess->errorString();
                        messageStr = odriveProcess->errorString();
                        return;
                    }
                    odriveProcess->close();
                    delete odriveProcess;

                    // ログファイルに書き込み
                    out << info.getFileName() + " を同期しました";

                    // 再帰FLGがtrueなら再帰的にフォルダを検索して同期リストに格納する
                    if (info.getRecursiveFlg()) {
                        QFileInfo fi;
                        fi.setFile(info.getFileName());

                        QString dirPath =  fi.path() + "/" + fi.baseName();
                        QDir dir = QDir(dirPath);
                        // フォルダなら中の同期ファイルを探す
                        if(dir.exists()){
                            // 対象のファイル名フィルタ
                            QStringList nameFilters;
                            nameFilters << "*.cloudf" << "*.cloud";
                            // ファイルフィルタ
                            QDir::Filters filters = QDir::Files;

                            QFileInfoList fil = dir.entryInfoList(nameFilters, filters);
                            QListIterator<QFileInfo> iterator_list( fil );
                            while (iterator_list.hasNext())
                            {
                                QString filePath = iterator_list.next().filePath();
                                qDebug() << filePath;
                                list_push(filePath, true);
                            }
                        }
                    }
                }
            } else {
                qDebug() << "odrive Pathが指定されていません。";
            }
        }

        msleep(1000);
    }
    stopped = false;
    messageStr = "Thread Stop!";
    appendFile.close();
}

/*!
 * \brief 停止用
 */
void SyncThread::stop()
{
    QMutexLocker locker(&mutex);
    stopped = true;//スレッドを止めるためのフラグを立てる
}

/*!
 * \brief 同期したいファイルを格納
 * \param fileName
 */
void SyncThread::list_push(QString fileName, bool recursiveFlg)
{
    QMutexLocker locker(&list_mutex);
    FileInfo info;
    info.setFileName(fileName);
    info.setRecursiveFlg(recursiveFlg);
    syncFileList.append(info);
}

/*!
 * \brief リストにある一番最初の同期ファイルの取り出し
 * \return
 */
FileInfo SyncThread::list_get()
{
    QMutexLocker locker(&list_mutex);
    return syncFileList.takeFirst();
}

/*!
 * \brief 同期リストか空かどうかを確認
 * \return
 */
bool SyncThread::list_isEmpty()
{
    QMutexLocker locker(&list_mutex);
    return syncFileList.isEmpty();
}
