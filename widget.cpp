#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QFileDialog>
#include <QJsonObject>
#include <QJsonDocument>
#include <QMessageBox>
#include <QDesktopWidget>

/*!
 * \brief コンストラクタ
 * \param parent
 */
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    // 画面のセットアップ
    ui->setupUi(this);

    // メニューの内部処理
    createActions();
    // メニューの作成
    createTrayIcon();

    // odriveのパス設定ファイルから取得する
    loadSetting();

    // 同期用のスレッドを開始
    syncThread = new SyncThread;
    syncThread->moveToThread(syncThread);   //  所有権を syncThread スレッドに移動
    syncThread->odrivePath = ui->txtPathodrive->text();
    syncThread->start();

    // プロセス実行変数
    agentProsess = new QProcess(this);

    // odrive agentのステータスを確認
    checkAgent();
}

/*!
 * \brief Widget::~Widget
 * デストラクタ
 */
Widget::~Widget()
{
    syncThread->stop();
    syncThread->wait();//終わるまで待つ
    syncThread->quit();

    //! プロセスが動いていたら殺す
    stopAgent();
    delete agentProsess;

    delete ui;
}

/*!
 * \brief Widget::createActions
 * イベントを作成する
 */
void Widget::createActions()
{
    syncDicrectryAction = new QAction("同期ファイルの選択", this);
    connect(syncDicrectryAction, SIGNAL(triggered()), this, SLOT(syncDirectry()));

    showWidget = new QAction("設定", this);
    connect(showWidget, SIGNAL(triggered()), this, SLOT(show()));

    quitAction = new QAction("終了", this);
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
}
/*!
 * \brief Widget::createTrayIcon
 * トレイアイコンを作成する
 */
void Widget::createTrayIcon()
{
    // メニューの作成
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(syncDicrectryAction);
    trayIconMenu->addAction(showWidget);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    // トレイアイコンの初期化
    trayIcon = new QSystemTrayIcon(QIcon(":img/odrive.png"), this);
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->show();
}

//!
//! \brief 設定ファイルの読み込み
//!
void Widget::loadSetting()
{
    if (QFile::exists(appfilePath))
    {
        // 存在する場合の処理
        QFile loadFile(appfilePath);
        // 開くことができなければ警告を出す
        if (!loadFile.open(QIODevice::ReadOnly))
        {
            qWarning("設定ファイルを開けませんでした。");
            return;
        }
        // 中身を取得
        QByteArray saveData = loadFile.readAll();

        // 取得したテキストからJsonを扱うためのオブジェクト生成
        QJsonObject json(QJsonDocument::fromJson(saveData).object());

        // Jsonオブジェクトから保存した値を取り出しする
        qDebug() << json[json_odrive_agentpath].toString();
        qDebug() << json[json_odrive_path].toString();
        qDebug() << json[json_odrive_mount_path].toString();
        ui->txtPathodriveagent->setText(json[json_odrive_agentpath].toString());
        ui->txtPathodrive->setText(json[json_odrive_path].toString());
        ui->txtPathodriveDir->setText(json[json_odrive_mount_path].toString());
        loadFile.close();
    }
}

//!
//! \brief 設定ファイルに内容を保存する
//!
void Widget::saveSetting()
{
    // QtでJsonを扱うためのオブジェクトを作製し、キーとそれに対応する値を保存する
    QJsonObject saveObject;
    saveObject[json_odrive_agentpath] = ui->txtPathodriveagent->text();
    saveObject[json_odrive_path] = ui->txtPathodrive->text();
    saveObject[json_odrive_mount_path] = ui->txtPathodriveDir->text();

    // 設定フォルダを作成する
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
    if (!dir.exists()) {
        dir.mkpath(".");
    }

    // QJsonオブジェクトをテキストの形にして、保存する
    QJsonDocument saveDoc(saveObject);
    QFile saveFile(appfilePath);
    saveFile.open(QIODevice::WriteOnly);
    saveFile.write(saveDoc.toJson());
    saveFile.close();
}

/*!
 * \brief Widget::syncDirectry
 * 同期ファイルの選択
 */
void Widget::syncDirectry()
{
    // 同期ファイル選択ダイアログ
    SyncFileDialog *syncDialog = new SyncFileDialog(this);
    // デフォルトスクリーンのサイズを取得
    QRect r = QApplication::desktop()->screenGeometry();
    // 中央に移動
    syncDialog->move((r.width() - width()) / 2, (r.height() - height()) / 2);

    syncDialog->setOdriveSyncPath(ui->txtPathodriveDir->text());

    if (syncDialog->exec() == QDialog::Accepted)
    {
        QString fileName = syncDialog->getFileName();
        if(!fileName.isEmpty()){
            syncThread->list_push(fileName, syncDialog->getRecursiveFlg());
        }
    }
    delete syncDialog;
}

/*!
 * \brief Widget::on_btnClose_clicked
 *  キャンセルボタン
 */
void Widget::on_btnClose_clicked()
{
    // odriveのパス設定ファイルから再取得する
    loadSetting();

    this->close();
}

/*!
 * \brief Widget::on_btnSave_clicked
 * OKボタン
 */
void Widget::on_btnSave_clicked()
{
    // 設定ファイルに内容を保存
    saveSetting();

    // 同期スレッドにodriveの実行ファイルの指定
    syncThread->odrivePath = ui->txtPathodrive->text();

    this->hide();
}

/*!
 * \brief Widget::on_btnSearch2_clicked
 * odrive 実行ファイルの参照ボタン
 */
void Widget::on_btnSearch2_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("odrive ファイル"),
                                                    QDir::homePath(),
                                                    tr("実行ファイル (*)"));
    qDebug() << fileName;
    ui->txtPathodrive->setText(fileName);
}

/*!
 * \brief Widget::on_btnSearch1_clicked
 * をodrive agent の参照ボタン
 */
void Widget::on_btnSearch1_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("odrive agent ファイル"),
                                                    QDir::homePath(),
                                                    tr("実行ファイル (*)"));
    qDebug() << fileName;
    ui->txtPathodriveagent->setText(fileName);
}

/*!
 * \brief Widget::on_btnSearch3_clicked
 * odrive 同期フォルダの参照ボタン
 */
void Widget::on_btnSearch3_clicked()
{
    QString dir_now = QDir::homePath();
    if (!ui->txtPathodriveDir->text().isEmpty()){
        dir_now = ui->txtPathodriveDir->text();
    }

    QString dir_new = QFileDialog::getExistingDirectory(this,
                                                        "odriveの同期フォルダ",
                                                        dir_now);
    qDebug() << dir_new;
    ui->txtPathodriveDir->setText(dir_new);
}

/*!
 * \brief Widget::on_btnThreadState_clicked
 * スレッドの状態を確認するボタン
 */
void Widget::on_btnThreadState_clicked()
{
    ui->lblThreadState->setText(syncThread->messageStr);//現在のスレッドの状態をラベルに表示
}

/*!
 * \brief odrive agentが起動しているかを確認する。
 * 起動してなければ、起動する。
 */
void Widget::checkAgent()
{
    QByteArray data;
    QString str;
    QProcess *odriveProcess = new QProcess(this);
    odriveProcess->start(ui->txtPathodrive->text() + " status");
    // 完了するまで待つ
    if (!odriveProcess->waitForFinished()) {
        qWarning() << odriveProcess->errorString();
        return;
    }
    // 実行結果を確認する
    while((data = odriveProcess->readLine()) != "") {
        str = QString(data.data());
        if (str.indexOf("desktop is running") != -1) {
            // 実行していない
            startAgent();
        } else {
            // 実行している
            notifyShow(tr("確認"), tr("odrive agentは実行です"));
            break;
        }
    }
    odriveProcess->close();
}

/*!
 * \brief odrive agentを実行する
 */
void Widget::startAgent()
{
    agentProsess->start(ui->txtPathodriveagent->text());
    notifyShow(tr("確認"), tr("odrive agentを起動しました"));
}

/*!
 * \brief odrive agentを停止する。
 */
void Widget::stopAgent()
{
    //! プロセスが動いていたら殺す
    if (agentProsess->state() == QProcess::Running) {
        agentProsess->execute(ui->txtPathodrive->text() + " shutdown");
        agentProsess->waitForFinished();
        notifyShow(tr("確認"), tr("odrive agentを終了しました"));
    }
}

/*!
 * \brief notifyにメッセージを送信して表示する
 * \param title タイトル
 * \param message メッセージ
 */
void Widget::notifyShow(QString title, QString message)
{
    notify_init(title.toUtf8().constData());
    NotifyNotification * MSG = notify_notification_new (title.toUtf8().constData(), message.toUtf8().constData(), "dialog-information");
    notify_notification_show (MSG, NULL);
    g_object_unref(G_OBJECT(MSG));
    notify_uninit();
}

/*!
 * \brief 再起動ボタン
 */
void Widget::on_btnRestart_clicked()
{
    if (ui->txtPathodriveagent->text().isEmpty()) {
        QMessageBox::warning(this, tr("確認"), tr("odriveagentの実行ファイルを指定してください。"));
        ui->txtPathodriveagent->setFocus();
        return;
    }
    if (ui->txtPathodrive->text().isEmpty()) {
        QMessageBox::warning(this, tr("確認"), tr("odriveの実行ファイルを指定してください。"));
        ui->txtPathodrive->setFocus();
        return;
    }
    // odrive Agentを再起動する。
    stopAgent();
    startAgent();
}
