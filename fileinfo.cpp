#include "fileinfo.h"

QString FileInfo::getFileName()
{
    return m_FileName;
}

void FileInfo::setFileName(QString filename)
{
    m_FileName = filename;
}

bool FileInfo::getRecursiveFlg()
{
    return m_RecursiveFlg;
}

void FileInfo::setRecursiveFlg(bool recursiveFlg)
{
    m_RecursiveFlg = recursiveFlg;
}
