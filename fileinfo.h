#ifndef FILEINFO_H
#define FILEINFO_H

#include <QString>

class FileInfo
{
public:
    QString getFileName();
    void setFileName(QString filename);
    bool getRecursiveFlg();
    void setRecursiveFlg(bool recursiveFlg);

private:
    QString m_FileName;
    bool m_RecursiveFlg;
};

#endif // FILEINFO_H
