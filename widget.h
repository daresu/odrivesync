#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QMenu>
#include <QSystemTrayIcon>
#include <QProcess>
#include <QStandardPaths>
#include "syncthread.h"
#include "syncfiledialog.h"
#undef signals
#include <libnotify/notify.h>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_btnClose_clicked();
    void on_btnSave_clicked();
    void on_btnSearch2_clicked();
    void on_btnSearch1_clicked();
    void on_btnSearch3_clicked();
    void syncDirectry();

    void on_btnThreadState_clicked();

    void on_btnRestart_clicked();

private:
    Ui::Widget *ui;
    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;
    QAction *syncDicrectryAction;
    QAction *showWidget;
    QAction *quitAction;
    const QString json_filename = "appfile.json";
    const QString json_odrive_agentpath = "odriveagentpath";
    const QString json_odrive_path = "odrivepath";
    const QString json_odrive_mount_path = "odrivemountpath";
    const QString appfilePath =
            QStandardPaths::writableLocation(QStandardPaths::DataLocation)
            + "/" + json_filename;
    SyncThread *syncThread;//<-----------ここ重要。
    QProcess *agentProsess;

    void createActions();
    void createTrayIcon();
    void loadSetting();
    void saveSetting();
    void checkAgent();
    void startAgent();
    void stopAgent();
    void notifyShow(QString title, QString message);
};

#endif // WIDGET_H
